
/*
 * grunt-ftp_stitchfix
 * https://github.com/jamesdyke/grunt-ftp_stitchfix
 *
 * Copyright (c) 2016 
 * Licensed under the MIT license.
 */
'use strict';

var fs = require('fs'),
  JSFtp = require("jsftp"),
  JSFtp = require('jsftp-mkdirp')(JSFtp),
  chalk = require('chalk'),
  walk = require('walk');

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('ftp_stitchfix', 'uploads to sf ftp and creates folders', function() {

    // Merge task-specific and/or target-specific options with these defaults.
    var srcFolder = this.files[0].orig.cwd;
    var destiationPath = '';
    var done = this.async();

    var Ftp = new JSFtp({
      host: this.options().host,
      port: this.options().port,
      user: this.options().user,
      pass: this.options().pass
    });

    var init = function() {
      putFilesUp();
    }

    var mkFolderPath = function(folder) {
      var re = /\/\/(.+)/;
      var year = new Date().getFullYear();
      var month = new Date().getMonth() + 1;
      month = (month < 10) ? '0' + month : month;
      var path = year + '/' + month + '/';


      if ((re.exec(folder)) !== null) {
        var res = {};
        res.folder = re.exec(folder)[1];
        res.destiationPath = path + res.folder;


      }
      return res;
    };

    var putFilesUp = function() {
      //console.log(srcFolder);
      var walker = walk.walk(srcFolder, {
        followLinks: false
      });
      walker.on('file', function(root, stat, next) {
        var folderPath = mkFolderPath(root);
        if (/\.png|\.jpg/.test(stat.name)) {
          Ftp.mkdirp(folderPath.destiationPath, function(err) {
            if (err) {
              throw err;

            } else {
              console.log(chalk.green('✔ ') + 'created destination FTP directory: ' + chalk.magenta(folderPath.destiationPath));
              
              var ftpDestination = srcFolder + folderPath.folder + stat.name;
                Ftp.put(srcFolder + folderPath.folder + '/' + stat.name, folderPath.destiationPath + '/' + stat.name, function(hadError) {
                  if (!hadError) {
                    console.log(chalk.green('✔ ') + 'Uploaded file to: ' + chalk.magenta(folderPath.destiationPath + '/' + stat.name));
                    next();
                  } else {
                    grunt.warn(hadError + ' in file ');
                    next();
                  }
                });
            }
          });
        } else {
          next();
        }
      });
      walker.on('end', function() {
        console.log('FTP upload complete')
        process.exit();
      })
    }
    init();
  });
};