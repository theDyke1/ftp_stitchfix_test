/*
 * grunt-ftp_stitchfix
 * https://github.com/jamesdyke/grunt-ftp_stitchfix
 *
 * Copyright (c) 2016 
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    jshint: {
      all: ['tasks/**/*.js']
    },
    // Configuration to be run (and then tested).
    ftp_stitchfix: {
      default_options: {
        options: {
          host: '',
          port: '',
          user: '',
          pass:''
        },
        files: [{
          expand: true,
          cwd: 'jamesTestA/',
          src: ['**/*.{png,jpg,jpeg,gif}']
        }]
      }
    }
    // Unit tests.
  });

  // Actually load this plugin's task(s).
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadTasks('tasks');

  // Whenever the "test" task is run, first clean the "tmp" dir, then run this
  // plugin's task(s), then test the result.

  grunt.registerTask('hint', ['jshint']);
  // By default, lint and run all tests.
  grunt.registerTask('default', ['ftp_stitchfix']);

};
